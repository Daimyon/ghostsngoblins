﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Walker : MonoBehaviour {

    Rigidbody2D rb;
    public float speed;
    public bool isFacingRight;
    public GameObject player;
    public Animator anim;
    public int health;


    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        player = GameObject.Find("Arthur(Clone)");

        if (player.transform.position.x > transform.position.x && !isFacingRight)
        {
            //face right
            flip();
        }
        else if (player.transform.position.x < transform.position.x && isFacingRight)
        {
            //face left
            flip();
        }

        if (speed <= 0)
        {
            speed = 2.0f;

            Debug.LogWarning("Default Speeding to " + speed);
        }

        rb = GetComponent<Rigidbody2D>();

        if (!rb)
        {
            Debug.LogWarning("No Rigidbody2D Found.");
        }
        health = 1;

    }

    // Update is called once per frame
    void Update () {

        if (player.transform.position.x > transform.position.x && !isFacingRight)
        {
            //face right
            flip();
        }
        else if (player.transform.position.x < transform.position.x && isFacingRight)
        {
            //face left
            flip();
        }

        // Check if enemy is facing right
        if (!isFacingRight)
            // Move Enemy Left
            rb.velocity = new Vector2(-speed, rb.velocity.y);
        else
            // Move Enemy Right
            rb.velocity = new Vector2(speed, rb.velocity.y);

    }


    private void OnCollisionEnter2D(Collision2D c)
    {

        // Check if enemy hits something else
        if (c.gameObject.tag == "Enemies" || c.gameObject.tag == "Coffins")
        {
            // Triggers Despawning animation
            anim.SetTrigger("Despawn");
        }
        if (c.gameObject.tag == "Projectile")

        {
            health--;

            if (health <= 0)
            {
                // Kill enemy
                // - Play Sound
                // - Trigger Respawn
                // - Play an animation
                // - etc

                // Enemy is dead, remove from scene
                anim.SetFloat("Death", 1);
                Destroy(c.gameObject);
                Game_Manager.instance.score = Game_Manager.instance.score + 100;
            }
        }
        if (c.gameObject.tag == "Player")
        {
            health--;

            if (health <= 0)
            {
                // Kill enemy
                // - Play Sound
                // - Trigger Respawn
                // - Play an animation
                // - etc

                // Enemy is dead, remove from scene
                anim.SetFloat("Death", 1);
            }
        }

    }

    void flip()
    {
        isFacingRight = !isFacingRight;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;
    }

    void KillOnAnimationEnd()
    {
        Destroy(gameObject);
    }

}
