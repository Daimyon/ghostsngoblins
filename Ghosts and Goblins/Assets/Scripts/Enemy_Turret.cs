﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Turret : MonoBehaviour
{

    public float range;
    public Transform player;
    public bool isFacingRight;
    public GameObject Arthur;
    private const int spawnDistance = 10;

    //Projectile Instantiation
    public Transform projectileSpawnPoint;
    public Projectile projectilePrefab;
    public float projectileForce;

    // Handles projectile mechanic (rate of fire)
    public float projectileFireRate;
    float timeSinceLastFire = 0.0f;

    // Enemy Health
    public int health;

    // Handles Animations
    public Animator anim;

    // Handles Audio
    public AudioSource audioSource;
    public AudioClip attack;
    public AudioClip death;

    // Use this for initialization
    void Start()
    {
        Arthur = GameObject.Find("Arthur(Clone)");
        player = Arthur.transform;

        if (!projectileSpawnPoint)
        {
            Debug.LogWarning("No ProjectileSpawnPoint Found on GameObject.");
        }
        if (!projectilePrefab)
        {
            Debug.LogWarning("No ProjectilePrefab found on GameObject.");
        }
        if (projectileForce == 0)
        {
            projectileForce = 2.0f;
            Debug.LogWarning("ProjectileForce was not set.  Defaulting to ." + projectileForce);
        }
        if (projectileFireRate == 0)
        {
            projectileFireRate = 2.0f;
            Debug.LogWarning("Projectile Fire Rate was not set.  Defaulting to ." + projectileFireRate);
        }
        if (health == 0)
        {
            health = 1;
            Debug.LogWarning("ProjectileForce was not set.  Defaulting to ." + health);
        }
        if (range == 0)
        {
            range = 5;
            Debug.LogWarning("Range was not set.  Defaulting to ." + range);
        }


    }

    // Update is called once per frame
    void Update()
    {
        if (player.position.x > transform.position.x && !isFacingRight)
        {
            //face right
            flip();
        }
        else if (player.position.x < transform.position.x && isFacingRight)
        {
            //face left
            flip();
        }

        // Check if enough time has passed before firing another projectile
        if (Time.time > timeSinceLastFire + projectileFireRate && Vector2.Distance(player.transform.position, this.transform.position) <= range)
        {
            fire();
            // Timestamp
            timeSinceLastFire = Time.time;
            anim.SetFloat("Attacking", 1);
            playSound(attack);

        }
        else
        {
            anim.SetFloat("Attacking", 0);
        }

    }

    void fire()
    {

        {

            Projectile temp =
            Instantiate(projectilePrefab, projectileSpawnPoint.transform.position, transform.rotation);

        if (!isFacingRight)
        {
            temp.speed = projectileForce * -1;
            temp.transform.localScale *= -1;
        }
        else
        {
            temp.speed = projectileForce * 1;
            temp.transform.localScale *= 1;
        }

    }
    }
    private void OnCollisionEnter2D(Collision2D c)
    {
        // Check if enemy was hit by a projectile
        if (c.gameObject.tag == "Projectile")
        {
            // Remove one health point
            health--;

            if (health <= 0)
            {
                // Kill enemy
                // - Play Sound
                // - Trigger Respawn
                // - Play an animation
                // - etc

                // Enemy is dead, remove from scene
                Destroy(gameObject);
                Game_Manager.instance.score = Game_Manager.instance.score + 500;
                playSound(death);
            }
        }
        if (c.gameObject.tag == "Player")
        {
            health--;

            if (health <= 0)
            {
                // Kill enemy
                // - Play Sound
                // - Trigger Respawn
                // - Play an animation
                // - etc

                // Enemy is dead, remove from scene
                anim.SetFloat("Death", 1);
            }
        }

    }
    void flip()
    {
        isFacingRight = !isFacingRight;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;
    }
    public void playSound(AudioClip clip, float volume = 1.0f)
    {
        // Assign volume to AudioSource volume
        audioSource.volume = volume;

        // Assign AudioClip to AudioSource clip
        audioSource.clip = clip;

        // Play assigned AudioClip through AudioSource on Character
        audioSource.Play();

    }
}
