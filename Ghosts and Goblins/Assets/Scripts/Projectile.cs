﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public float speed;
    public float lifeTime;


    // Use this for initialization
    void Start()
    {
        if (lifeTime <= 0)
        {
            lifeTime = 1.5f;
        }

        GetComponent<Rigidbody2D>().velocity =
            new Vector2(speed, 0);

        Destroy(gameObject, lifeTime);

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionEnter2D(Collision2D c)
    {
        Destroy(gameObject);
    }
}
 