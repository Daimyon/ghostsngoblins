﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Boss : MonoBehaviour
{

    public Rigidbody2D rb;
    public float speed;
    public float range;
    public Transform player;
    public GameObject Arthur;
    public float jumpForce;
    public GameObject keyPrefab;

    //Projectile Instantiation
    public Transform projectileSpawnPoint;
    public Projectile projectilePrefab;
    public float projectileForce;
    public Animator anim;
    public bool isFacingRight;

    // Handles projectile mechanic (rate of fire)
    public float projectileFireRate;
    float timeSinceLastFire = 0.0f;

    // Handles Boss Jumps
    public float jumpRate;
    float timeSinceLastJump = 0.0f;

    // Handles Audio
    public AudioSource audioSource;
    public AudioClip attack;
    public AudioClip death;
    public AudioClip hit;


    // Enemy Health
    public int health;

    // Use this for initialization
    void Start()
    {
        Arthur = GameObject.Find("Arthur(Clone)");
        player = Arthur.transform;

        rb = GetComponent<Rigidbody2D>();
        if (!rb)
        {
            Debug.LogWarning("No Rigidbody2D Found.");
        }
        {
            speed = 0.5f;

            Debug.LogWarning("Default Speeding to " + speed);
        }
        if (!projectileSpawnPoint)
        {
            Debug.LogWarning("No ProjectileSpawnPoint Found on GameObject.");
        }
        if (!projectilePrefab)
        {
            Debug.LogWarning("No ProjectilePrefab found on GameObject.");
        }
        if (projectileForce == 0)
        {
            projectileForce = 2.0f;
            Debug.LogWarning("ProjectileForce was not set.  Defaulting to ." + projectileForce);
        }
        if (health == 0)
        {
            health = 1;
            Debug.LogWarning("ProjectileForce was not set.  Defaulting to ." + health);

        }
        if (jumpForce <= 0)
        {
            jumpForce = 3.6f;

            Debug.LogWarning("Default jumpForce to " + jumpForce);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Time.time > timeSinceLastFire + projectileFireRate && Vector2.Distance(player.transform.position, this.transform.position) <= range)
        {
            fire();
            // Timestamp
            timeSinceLastFire = Time.time;
            anim.SetFloat("Attack", 1);
            playSound(attack);

        }
        else
        {
            anim.SetFloat("Attack", 0);
        }
        if (Time.time > timeSinceLastJump + jumpRate)
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            // Timestamp
            timeSinceLastJump = Time.time;
        }
            // Check if enemy is facing right
            if (!isFacingRight)
                // Move Enemy Left
                rb.velocity = new Vector2(-speed, rb.velocity.y);
            else
                // Move Enemy Right
                rb.velocity = new Vector2(speed, rb.velocity.y);
        }
    
    void fire()
    {

        {

            Projectile temp =
            Instantiate(projectilePrefab, projectileSpawnPoint.transform.position, transform.rotation);


            temp.speed = projectileForce * -1;
            temp.transform.localScale *= 1;

        }
    }
    private void OnCollisionEnter2D(Collision2D c)
    {

        if (c.gameObject.tag == "Projectile")

        {
            health--;
            Destroy(c.gameObject);
            playSound(hit);
            if (health <= 0)
            {
                // Kill enemy
                // - Play Sound
                // - Trigger Respawn
                // - Play an animation
                // - etc
                speed = 0;
                projectileFireRate = 1000000f;
                speed = 0;
                jumpRate = 100000f;
                // Enemy is dead, remove from scene
                anim.SetFloat("Death", 1);
                Destroy(c.gameObject);
                Game_Manager.instance.score = Game_Manager.instance.score + 5000;
                rb.gravityScale = 0f;
                rb.mass = 0f;
                playSound(death);



                
            }
        }
        if (c.gameObject.tag == "Obstacle")
        {
            flip();
        }
    }
    void flip()
    {
        isFacingRight = !isFacingRight;

    }
    public void playSound(AudioClip clip, float volume = 1.0f)
    {
        // Assign volume to AudioSource volume
        audioSource.volume = volume;

        // Assign AudioClip to AudioSource clip
        audioSource.clip = clip;

        // Play assigned AudioClip through AudioSource on Character
        audioSource.Play();

    }
    public void Despawn()
    {
        Destroy(gameObject);
    }
    public void keyDrop()
    {
        Debug.Log("KeyDrop");
        Instantiate(keyPrefab, projectileSpawnPoint.transform.position, transform.rotation);
        Debug.Log("dropped");

    }
}

