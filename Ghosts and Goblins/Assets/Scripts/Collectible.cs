﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {

    int _pointValue;

	// Use this for initialization
	void Start () {
        pointValue = 10;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public int pointValue
    {
        get { return _pointValue; }
        set { _pointValue = value; }
    }
}
