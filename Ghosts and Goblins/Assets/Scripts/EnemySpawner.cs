﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public GameObject enemyPrefab;
    float randX;
    Vector2 spawnLocation;
    public float spawnRate;
    float nextSpawn = 0.0f;

    private void Start()
    {

    }


    // Update is called once per frame
    void Update () {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            randX = Random.Range(transform.position.x - 11f, transform.position.x + 11f);
            spawnLocation = new Vector2(randX, transform.position.y);
            Instantiate(enemyPrefab, spawnLocation, Quaternion.identity);
        }
		
	}
}
