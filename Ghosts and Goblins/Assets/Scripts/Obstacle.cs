﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    // Holds starting health of 'Obstacle'
    public int health;

    // Used to change size of HealthBar size
    public RectTransform healthBar;
    public float healthScale;

	// Use this for initialization
	void Start () {

        // Check if 'Health' was set at Start
        if(health <= 0)
        {
            // Assign a default value of 5 to 'health'
            health = 5;

            Debug.Log("Health was not set.  Defaulting to " + health);
        }
        healthScale = healthBar.sizeDelta.x / health;
	}

    private void OnCollisionEnter2D(Collision2D c)
    {
        if(c.gameObject.tag == "Projectile")
        {
            health--;

            healthBar.sizeDelta = new Vector2(health * healthScale, healthBar.sizeDelta.y);
        }
        if(health <= 0)
        {
            Destroy(gameObject);
        }
    }

}
