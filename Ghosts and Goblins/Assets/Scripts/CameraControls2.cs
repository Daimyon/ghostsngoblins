﻿using UnityEngine;
using System.Collections;


public class CameraControls2 : MonoBehaviour
{

    public GameObject player;
    public Collider2D boundary;
    Camera cam;
    [Range(1, 10)]
    public float scrollSpeed = 3.0f;
    [Space(10)]
    public Vector2 movementWindowSize = new Vector2(200, 100);
    private Vector2 windowOffset;
    private GameObject boundaryParent;


    public bool limitCameraMovement = true;
    public bool activeTracking = true;

    private Vector3 cameraPosition;
    private Vector3 playerPosition;
    private Vector3 previousPlayerPosition;
    private Rect windowRect;

    void Start()
    {
        boundaryParent = GameObject.Find("GameBoundaries");
        player = GameObject.Find("Arthur(Clone)");
        boundary = boundaryParent.GetComponent<Collider2D>();
        cam = Camera.main;

        cameraPosition = transform.position;

        if (player == null)
            Debug.Log("Player GameObject must be named 'Character'.");
        if (boundary == null)
            Debug.Log("No Collider2D on the boundary GameObject.");
        if (boundaryParent == null)
            Debug.Log("Either no EmptyObject named GameBoundaries created, or named incorrectly.  Please name GameObject 'GameBoundaries'.");
        if (boundary.isTrigger == false)
            Debug.Log("GameBoundaries collider not set to Trigger.");

        previousPlayerPosition = player.transform.position;

        // Used to determine the anchor point which the script will use to draw the limit boxes.
        float windowAnchorX = cameraPosition.x - movementWindowSize.x / 2 + windowOffset.x;
        float windowAnchorY = cameraPosition.y - movementWindowSize.y / 2 + windowOffset.y;

        // Determines the size of the area inside the camera that the player can move.
        windowRect = new Rect(windowAnchorX, windowAnchorY, movementWindowSize.x, movementWindowSize.y);

    }


    void LateUpdate()
    {
        //Updates the camera position based on player location
        CameraUpdate();

    }

    void CameraUpdate()
    {
        playerPosition = player.transform.position;

        // Only worry about updating the camera based on player position if the player has actually moved.
        if (activeTracking && playerPosition != previousPlayerPosition)
        {

            cameraPosition = transform.position;

            // Get the distance of the player from the camera.
            Vector3 playerPositionDifference = playerPosition - previousPlayerPosition;

            // Move the camera this direction, speed of scroll based off scroll multiplier.
            Vector3 multipliedDifference = playerPositionDifference * scrollSpeed;

            cameraPosition += multipliedDifference;

            // Updating our movement window root location based on the current camera position
            windowRect.x = cameraPosition.x - movementWindowSize.x / 2 + windowOffset.x;
            windowRect.y = cameraPosition.y - movementWindowSize.y / 2 + windowOffset.y;

            // Prevents the player from leaving the camera boundaries
            if (!windowRect.Contains(playerPosition))
            {
                Vector3 positionDifference = playerPosition - cameraPosition;
                positionDifference.x -= windowOffset.x;
                positionDifference.y -= windowOffset.y;

                // Determines the distance to snap to the player.
                cameraPosition.x += DifferenceOutOfBounds(positionDifference.x, movementWindowSize.x);


                cameraPosition.y += DifferenceOutOfBounds(positionDifference.y, movementWindowSize.y);

            }

            // Controls camera movement limits.
            float height = 2.0f * cam.orthographicSize;
            float width = height * cam.aspect;
            float halfCam = width / 2.0f;

                if (limitCameraMovement)
                {
                    cameraPosition.y = Mathf.Clamp(cameraPosition.y, boundary.bounds.min.y + (height / 2), boundary.bounds.max.y - (height / 2));
                    cameraPosition.x = Mathf.Clamp(cameraPosition.x, boundary.bounds.min.x + halfCam, boundary.bounds.max.x - halfCam);

            
            }
            transform.position = cameraPosition;

        }

        previousPlayerPosition = playerPosition;
    }


    // Determines how far the player has overshot the camera.
    static float DifferenceOutOfBounds(float differenceAxis, float windowAxis)
    {
        float difference;

        if (Mathf.Abs(differenceAxis) <= windowAxis / 2)
            difference = 0f;
        else
            difference = differenceAxis - (windowAxis / 2) * Mathf.Sign(differenceAxis);
        return difference;

    }

    public void MoveCamera(Vector3 targetPosition, float moveSpeed)
    {
        StartCoroutine(MoveToPosition(targetPosition, moveSpeed));
    }
    IEnumerator MoveToPosition(Vector3 targetPosition, float moveSpeed)
    {
        activeTracking = false;

        // Keeps the camera on the Z position for 2D usage.
        targetPosition.z = transform.position.z;

        while (transform.position != targetPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
            yield return 0;
        }

        activeTracking = true;
    }

}
