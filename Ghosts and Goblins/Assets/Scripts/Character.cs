﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Character : MonoBehaviour
{

    Rigidbody2D rb;
    public Rigidbody2D rb2;
    public Collider2D arthurCollider;
    public float speed;
    public float jumpForce;
    public bool isGrounded;
    public LayerMask isGroundLayer;
    public Transform groundCheck;
    public float groundCheckRadius;
    public float rateFire = 1.0f;
    private float nextFire = 0.0f;
    public Animator anim;
    public Vector2 levelEnd;


    // AudioClips for Arthur
    public AudioSource audioSource;
    public AudioClip attack;
    public AudioClip hit;
    public AudioClip death;
    public AudioClip itemPickup;
    public AudioClip bgm;

    //Handles character flipping
    public bool isFacingRight;

    //Projectile Instantiation
    public Transform projectileSpawnPoint;
    public Projectile projectilePrefab;
    public float projectileForce;

    int _lives;
    public int health;

    // Use this for initialization
    void Start()
    {
        arthurCollider = GetComponent<Collider2D>();
        isFacingRight = true;

        rb = GetComponent<Rigidbody2D>();

        if (!rb)
        {
            Debug.LogWarning("No Rigidbody2D Found.");
        }
        if (!rb2)
        {
            rb2 = GetComponent<Rigidbody2D>();
        }
        if (speed <= 0)
        {
            speed = 1.6f;

            Debug.LogWarning("Default Speeding to " + speed);
        }

        if (jumpForce <= 0)
        {
            jumpForce = 3.6f;

            Debug.LogWarning("Default jumpForce to " + jumpForce);
        }

        if (groundCheckRadius <= 0)
        {
            groundCheckRadius = 0.2f;

            Debug.LogWarning("Default groundCheckRadius to " + groundCheckRadius);
        }

        if (!groundCheck)
        {
            Debug.LogWarning("No groundCheck Found.");
        }

        anim = GetComponent<Animator>();

        if (!anim)
        {
            Debug.LogWarning("No Animator Found.");
        }
        if (!projectileSpawnPoint)
        {
            Debug.LogWarning("No ProjectileSpawnPoint Found on GameObject.");
        }
        if (!projectilePrefab)
        {
            Debug.LogWarning("No ProjectilePrefab found on GameObject.");
        }
        if (projectileForce == 0)
        {
            projectileForce = 3.0f;
            Debug.LogWarning("ProjectileForce was not set.  Defaulting to ." + projectileForce);
        }
        lives = 0;
        health = 2;
    }




    void Update()
    {

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, isGroundLayer);

        float moveValue = Input.GetAxisRaw("Horizontal");



        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }

        if (!isGrounded)
        {
            anim.SetFloat("MoveValue", 0);
            anim.SetFloat("JumpValue", 1);
        }
        else if (!isGrounded && moveValue > 0.1)
        {
            anim.SetFloat("MoveValue", 0);
            anim.SetFloat("JumpValue", 1);
        }
        else
        { anim.SetFloat("JumpValue", 0); }

        if (Input.GetKey(KeyCode.LeftControl) && Time.time > nextFire)
        {
            nextFire = Time.time + rateFire;
            anim.SetFloat("Firing", 1);
            Debug.Log("Fire");
            fire();
            playSound(attack);
        }

        else
        {
            anim.SetFloat("Firing", 0);
        }

        if (Input.GetKey(KeyCode.S) && moveValue == 0)
        {
            anim.SetFloat("Crouch", 1);
        }
        else
        {
            anim.SetFloat("Crouch", 0);
        }
        rb.velocity = new Vector2(speed * moveValue, rb.velocity.y);

        anim.SetFloat("MoveValue", Mathf.Abs(moveValue));

        if ((isFacingRight && moveValue < 0) || (!isFacingRight && moveValue > 0))
        {
            flip();
        }
    }

    void fire()
    {
        Projectile temp =
            Instantiate(projectilePrefab, projectileSpawnPoint.position, projectileSpawnPoint.rotation);
        if (isFacingRight)
        {
            temp.speed = projectileForce;
        }
        else
        {
            temp.speed = projectileForce * -1;
            temp.transform.localScale *= -1;
        }

    }
    void flip()
    {
        isFacingRight = !isFacingRight;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;
    }
    // Give access to private variables (instance variables)
    // - Not needed if using public variables
    public int lives
    {
        get { return _lives; }
        set
        {
            _lives = value;
            Debug.Log("Lives changed to " + _lives);
        }
    }
    private void OnCollisionEnter2D(Collision2D c)
    {

        if (c.gameObject.tag == "Enemies" || c.gameObject.tag == "EnemyProjectile")

        {
            health--;
            rb2.AddForce(transform.up * 2, ForceMode2D.Impulse);
            anim.SetTrigger("Hit");
            playSound(hit);


        }
        else if (c.gameObject.tag == "DeadZone")
        {

            Reload();
        }

        if (health <= 0)
        {
            GetComponent<Collider2D>().isTrigger = true;
            rb.mass = 0;
            anim.SetTrigger("Death");
            speed = 0;
            Game_Manager.instance.lives = Game_Manager.instance.lives - 1;
            SoundManager.instance.playSingleSong(death);

            Game_Manager.instance.score = 0;
        }
        if (c.gameObject.tag == "LevelKey")
        {
            Destroy(c.gameObject);
            anim.SetFloat("Victory", 1);
            speed = 0f;
            anim.SetFloat("moveValue", 0);
            anim.SetFloat("jumpValue", 0);
        }


    }


    private void OnCollisionStay2D(Collision2D c2)
    {
        if (c2.gameObject.tag == "MovingPlatform")
        {
            transform.parent = c2.transform;
        }
        else
        {
            transform.parent = null;
        }

    }
    public void playSound(AudioClip clip, float volume = 1.0f)
    {
        // Assign volume to AudioSource volume
        audioSource.volume = volume;

        // Assign AudioClip to AudioSource clip
        audioSource.clip = clip;

        // Play assigned AudioClip through AudioSource on Character
        audioSource.Play();
    }
    public void Reload()
    {
        if (Game_Manager.instance.lives > 0)
        {
            string currentScene = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentScene);
            Game_Manager.instance.score = 0;
        }
    }
    private void OnTriggerStay2D(Collider2D c3)
    {
        if (c3.gameObject.tag == "Ladder")
        {
            if (Input.GetKey(KeyCode.W))
            {
                rb.gravityScale = 0.0f;
                anim.SetFloat("Climb", 1);
                transform.position = new Vector2(transform.position.x, transform.position.y + 0.05f);
                this.GetComponent<Collider2D>().isTrigger = true;

            }
            if (Input.GetKey(KeyCode.S))
            {
                rb.gravityScale = 0.0f;
                anim.SetFloat("Climb", 1);
                transform.position = new Vector2(transform.position.x, transform.position.y - 0.05f);
                this.GetComponent<Collider2D>().isTrigger = true;
            }

        }
    }
    private void OnTriggerExit2D(Collider2D c4)
    {
        if (c4.gameObject.tag == "Ladder")
        {
            rb.gravityScale = 1f;
            anim.SetFloat("Climb", 0);
            this.GetComponent<Collider2D>().isTrigger = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D c5)
    {
        if (c5.gameObject.tag == "Collectable")
        {
            playSound(itemPickup);
            Game_Manager.instance.score = Game_Manager.instance.score + 1000;
            Destroy(c5.gameObject);
        }
    } 

    public void endGame()
    {
        SoundManager.instance.stopSingleSong();
        SceneManager.LoadScene("Game Over");
    }
}
