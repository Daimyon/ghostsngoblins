﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]


public class ExtraLife : MonoBehaviour {

    Rigidbody2D rb;
    BoxCollider2D bc;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();

        rb.gravityScale = 0;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;

        bc.isTrigger = true;
		
	}
    
    private void OnTriggerEnter2D(Collider2D c)
    {
        if(c.gameObject.tag == "Player")
        {
           Character cc = c.gameObject.GetComponent<Character>();
           
            if(cc)
            {
                cc.lives++;
            }

            Destroy(gameObject);
        }
    }
}
