﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    static SoundManager _instance = null;

    public AudioSource sfxSource;
    public AudioSource musicSource;

    // Use this for initialization
    void Start () {
        if (instance)
            DestroyImmediate(gameObject);
        else
        {
            instance = this;

            DontDestroyOnLoad(this);
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public static SoundManager instance
    {
        get { return _instance; }
        set { _instance = value; }
    }
    public void playSingleSound(AudioClip clip, float volume = 1.0f)
    {
        // Assign volume to AudioSource volume
        sfxSource.volume = volume;

        // Assign AudioClip to AudioSource clip
        sfxSource.clip = clip;

        // Play assigned AudioClip through AudioSource on Character
        sfxSource.Play();
    }
    public void playSingleSong(AudioClip clip, float volume = 1.0f)
    {
        // Assign volume to AudioSource volume
        musicSource.volume = volume;

        // Assign AudioClip to AudioSource clip
        musicSource.clip = clip;

        // Play assigned AudioClip through AudioSource on Character
        musicSource.Play();
    }
    public void stopSingleSound()
    {
        // Assign volume to AudioSource volume
        sfxSource.Stop();
    }
    public void stopSingleSong()
    {
        // Assign volume to AudioSource volume
        musicSource.Stop();
    }
}
