﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour {


    public int spawnLocation;
    // public Transform spawnLocation;
    // public string spawnLocation;
    // public GameObject spawnLocation;
    // public Vector3 spawnLocation;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Awake () {
        if (spawnLocation < 0)
            spawnLocation = 0;

        // Call 'spawnPlayer()' from Game_Manager
        Game_Manager.instance.spawnPlayer(spawnLocation);

        Game_Manager.instance.scoreText = GameObject.Find("Text_Score").GetComponent<Text>();

        Game_Manager.instance.scoreText.text = "" + Game_Manager.instance.score;

        Game_Manager.instance.livesText = GameObject.Find("Text_Lives").GetComponent<Text>();

        Game_Manager.instance.livesText.text = "" + Game_Manager.instance.lives;

        Game_Manager.instance.topScoreText = GameObject.Find("Text_TopScoreNo").GetComponent<Text>();

        Game_Manager.instance.topScoreText.text = "" + Game_Manager.instance.topScore;

    }
}
