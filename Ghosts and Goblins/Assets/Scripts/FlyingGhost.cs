﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingGhost : MonoBehaviour
{

    public Rigidbody2D rb;
    public float speed;

    //Projectile Instantiation
    public Transform projectileSpawnPoint;
    public Projectile projectilePrefab;
    public float projectileForce;
    public Animator anim;


    // Enemy Health
    public int health;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (!rb)
        {
            Debug.LogWarning("No Rigidbody2D Found.");
        }
        {
            speed = 1.0f;

            Debug.LogWarning("Default Speeding to " + speed);
        }
        if (!projectileSpawnPoint)
        {
            Debug.LogWarning("No ProjectileSpawnPoint Found on GameObject.");
        }
        if (!projectilePrefab)
        {
            Debug.LogWarning("No ProjectilePrefab found on GameObject.");
        }
        if (projectileForce == 0)
        {
            projectileForce = 2.0f;
            Debug.LogWarning("ProjectileForce was not set.  Defaulting to ." + projectileForce);
        }
        if (health == 0)
        {
            health = 1;
            Debug.LogWarning("ProjectileForce was not set.  Defaulting to ." + health);

        }
        StartCoroutine(wait());
    }

    // Coroutine to wait before firing
    IEnumerator wait()
    {

        yield return new WaitForSeconds(1);

        fire();
    }
    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(-speed, rb.velocity.y);

    }
    void fire()
    {

        {

            Projectile temp =
            Instantiate(projectilePrefab, projectileSpawnPoint.transform.position, transform.rotation);


            temp.speed = projectileForce * -1;
            temp.transform.localScale *= 1;

        }
    }
    private void OnCollisionEnter2D(Collision2D c)
    {

        if (c.gameObject.tag == "Projectile")

        {
            health--;
            Destroy(c.gameObject);
            if (health <= 0)
            {
                // Kill enemy
                // - Play Sound
                // - Trigger Respawn
                // - Play an animation
                // - etc

                // Enemy is dead, remove from scene
                anim.SetFloat("Death", 1);
                Destroy(c.gameObject);
                Game_Manager.instance.score = Game_Manager.instance.score + 100;
            }
        }
    }

}
   