﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostSpawner : MonoBehaviour {

    public GameObject enemyPrefab;
    float randX;
    float randY;
    Vector2 spawnLocation;
    public float spawnRate;
    float nextSpawn = 0.0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            randY = Random.Range(transform.position.y + 0.5f, transform.position.y - 0.5f);
            randX = Random.Range(transform.position.x + 1, transform.position.x - 1);
            spawnLocation = new Vector2(randX, randY);
            Instantiate(enemyPrefab, spawnLocation, transform.rotation);
        }

    }
}
