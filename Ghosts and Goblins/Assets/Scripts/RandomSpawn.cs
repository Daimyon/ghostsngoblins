﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour {

    GameObject spawn;
    public GameObject[] ranSpawn;

    void Start () {
        ranSpawn = GameObject.FindGameObjectsWithTag("Collectible");

        spawn = ranSpawn[Random.Range(0, ranSpawn.Length)];
        Instantiate(spawn, this.transform.position, this.transform.rotation);
    }
	
}
