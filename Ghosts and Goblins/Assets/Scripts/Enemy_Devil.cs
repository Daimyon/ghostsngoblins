﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Devil : MonoBehaviour {

    Rigidbody2D rb;
    public float speed;
    public bool isFacingRight;
    public GameObject player;
    public Animator anim;
    public int health;
    public int range;
    public bool inRange;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        player = GameObject.Find("Arthur(Clone)");

        if (player.transform.position.x > transform.position.x && !isFacingRight)
        {
            //face right
            flip();
        }
        else if (player.transform.position.x < transform.position.x && isFacingRight)
        {
            //face left
            flip();
        }

        if (speed <= 0)
        {
            speed = 2.0f;

            Debug.LogWarning("Default Speeding to " + speed);
        }

        rb = GetComponent<Rigidbody2D>();

        if (!rb)
        {
            Debug.LogWarning("No Rigidbody2D Found.");
        }
        health = 5;

        if (range == 0)
        {
            range = 5;
            Debug.LogWarning("Range was not set.  Defaulting to ." + range);
        }

    }

    // Update is called once per frame
    void Update () {

        if (Vector2.Distance(player.transform.position, transform.position) <= range)
        {
            inRange = true;
            anim.SetTrigger("Active");
        }
        else
        {
            inRange = false;
        }

        if (inRange)
        {
            // Check if enemy is facing right
            if (!isFacingRight)
                // Move Enemy Left
                rb.velocity = new Vector2(-speed, rb.velocity.y);
            else
                // Move Enemy Right
                rb.velocity = new Vector2(speed, rb.velocity.y);
        }
        
    }


    private void OnCollisionEnter2D(Collision2D c)
    {

        // Check if enemy hits something else
        if (c.gameObject.tag == "Enemies" || c.gameObject.tag == "Coffins" || c.gameObject.tag == "Player")
        {

            flip();
        }
        if (c.gameObject.tag == "Projectile")

        {
            health--;

            if (health <= 0)
            {
                // Kill enemy
                // - Play Sound
                // - Trigger Respawn
                // - Play an animation
                // - etc

                // Enemy is dead, remove from scene
                anim.SetTrigger("Death");
                Destroy(c.gameObject);
                Game_Manager.instance.score = Game_Manager.instance.score + 1000;
            }
        }
        if (c.gameObject.tag == "Player")
        {
            health--;

            if (health <= 0)
            {
                // Kill enemy
                // - Play Sound
                // - Trigger Respawn
                // - Play an animation
                // - etc

                // Enemy is dead, remove from scene
                anim.SetFloat("Death", 1);
            }
        }

    }

    void flip()
    {
        isFacingRight = !isFacingRight;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;
    }

    void KillOnAnimationEnd()
    {
        Destroy(gameObject);
    }

}
