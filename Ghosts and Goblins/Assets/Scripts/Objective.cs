﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective : MonoBehaviour {

    public GameObject player;

    public GameObject[] collectibles;

    public int numberOfCollectibles;

    public GameObject finishLine;

	// Use this for initialization
	void Start () {

        player = GameObject.Find("Mario");

        // player = GameObject.FindGameObjectWithTag("Player");

        collectibles = GameObject.FindGameObjectsWithTag("Collectible");
        numberOfCollectibles = collectibles.Length;

        finishLine = GameObject.Find("Objective");

	}
	
	// Update is called once per frame
	void Update () {

        if(player && finishLine)
        {
            float distanceToFinish = Vector2.Distance(
                player.transform.position, finishLine.transform.position);

            distanceToFinish = (finishLine.transform.position
                - player.transform.position).magnitude;

        }
		
	}
}
