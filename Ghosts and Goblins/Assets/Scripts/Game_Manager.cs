﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game_Manager : MonoBehaviour
{

    static Game_Manager _instance = null;

    public GameObject playerPrefab;

    int _score;
    public Text scoreText;
    public int _lives;
    public Text livesText;
    public int _topScore;
    public Text topScoreText;
    public AudioClip lvl1Music;
    public AudioClip mapMusic;

    public AudioClip gameOverMusic;


    // Use this for initialization
    void Start()
    {
        if (instance)
            DestroyImmediate(gameObject);
        else
        {
            instance = this;

            DontDestroyOnLoad(this);
        }
        // Assign a starting score
        score = 0;
        // Assign starting lives
        lives = 3;
        // Assign Starting Top Score
        topScore = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            lives--;
            Debug.Log(lives);
        }

        if (lives <= 0)
        {
            lives = 3;
            SceneManager.LoadScene("Game Over");
            SoundManager.instance.playSingleSong(gameOverMusic);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().name == "Level 1")
                SceneManager.LoadScene("TitleScreen");
            else if (SceneManager.GetActiveScene().name == "TitleScreen")
                SceneManager.LoadScene("Level 1");
            else if (SceneManager.GetActiveScene().name == "Game Over")
                SceneManager.LoadScene("TitleScreen");

        }
        if (Input.GetKeyDown(KeyCode.Return))
            {
            if (SceneManager.GetActiveScene().name == "TitleScreen")
            {
                StartGame();
            }
            if (SceneManager.GetActiveScene().name == "Map")
            {
                SceneManager.LoadScene("Level 1");
            }
        }
        if (score > topScore)
            topScore = score;
    } 

    // Gets called to start the game
    public void StartGame()
    {
        // Loads Level 1 Scene
        SceneManager.LoadScene("Map");
        SoundManager.instance.playSingleSong(mapMusic);
    }


    // Gets called to quit the game
    public void QuitGame()
    {
        // Quits game (only works on EXE, not in Editor)
        Debug.Log("Quit Game");

        Application.Quit();
    }

    // Called when the 'Character' is spawned
    public void spawnPlayer(int spawnLocation)
    {
        // Requires spawnPoint to be named (SceneName)_(Number)
        // - Level 1_0
        string spawnPointName = SceneManager.GetActiveScene().name
            + "_" + spawnLocation;

        // Find location to spawn 'Character' at
        Transform spawnPointTransform = GameObject.Find(spawnPointName).GetComponent<Transform>();

        // Instantiate (Create) 'Character' GameObject
        Instantiate(playerPrefab, spawnPointTransform.position, spawnPointTransform.rotation);
        SoundManager.instance.playSingleSong(lvl1Music);
    }



    public static Game_Manager instance
    {
        get { return _instance; }
        set { _instance = value; }
    }

    public int score
    {
        get { return _score; }
        set
        {
            _score = value;
            if (scoreText)
                scoreText.text = "" + score;
        }
    }

    public int lives
    {
        get { return _lives; }
        set
        {
            _lives = value;
            if (livesText)
                livesText.text = "" + lives;
            Debug.Log("Lives changed to " + _lives);
        }

}
    public int topScore
    {
        get { return _topScore; }
        set
        {
            _topScore = value;
            if (topScoreText)
                topScoreText.text = "" + topScore;

        }

    }



}