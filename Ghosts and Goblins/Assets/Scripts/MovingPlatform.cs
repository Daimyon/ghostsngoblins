﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    public bool moveHorizontal;
    public bool moveVertical;
    [Range(0, 10)]
    public float platformSpeed;
    private float speed;
    public int xMoveDistance;
    public int yMoveDistance;
    public float xCenter;

    private void Start()
    {

        xCenter = transform.position.x;
    } 

    // Update is called once per frame
    void Update () {
        speed = platformSpeed * 10;
        if (moveHorizontal == true)
        {
            transform.position = new Vector3(xCenter + Mathf.PingPong(Time.time * speed, xMoveDistance) - xMoveDistance / 2, transform.position.y, transform.position.z);
        }
        if (moveVertical == true)
        {
            transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time* speed, yMoveDistance) - (yMoveDistance / 2), transform.position.z);
        }
    }
}
